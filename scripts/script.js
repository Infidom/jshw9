function addList(array, parent = document.body) {
    let list = document.createElement('ul');
    array.forEach(element => {
        let item = document.createElement('li');
        item.innerText = element;
        list.appendChild(item);
    });
    parent.appendChild(list);
}

addList(['Kyiv', 'Paris', 'London', 'Berlin', 'Vienn']);